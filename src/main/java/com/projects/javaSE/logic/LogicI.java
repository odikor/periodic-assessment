package com.projects.javaSE.logic;

import java.sql.SQLException;

public interface LogicI<T> {
    boolean add(T t);
    boolean edit(T t);
    boolean delete(T t) throws SQLException, ClassNotFoundException;
    boolean read(T t) throws SQLException, ClassNotFoundException;
}
