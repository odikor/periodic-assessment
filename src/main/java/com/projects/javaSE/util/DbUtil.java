package com.projects.javaSE.util;

import com.projects.javaSE.model.Student;

import java.sql.*;

    public class DbUtil {
        /**
         * Database connection requirements
         *  - Register connection
         *  - Open connection
         *  - Execute Query
         *  - CLose connection
         * */
        //1. Create Connection
        private final Connection connection;
        private Statement statement;
        String className = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3305/javasedemo";
        String root = "root";
        String password = "";

        public DbUtil() throws ClassNotFoundException, SQLException {
            Class.forName(className);
            connection = DriverManager.getConnection(url, root, password);
        }
        //2. Read data from Database
        public ResultSet readData(String query) throws SQLException { /**@Returns a ResultSet - Takes in a query, Example is Select, search*/
            statement = connection.createStatement();
            return statement.executeQuery(query);
        }
        /**@Insert Values into the database*/
        public int updateData(String query) throws SQLException {
            statement = connection.createStatement();
            return statement.executeUpdate(query); /**@Check_out the class object executeupdate (Insert, update, Delete,Drop)
             *@Returns int or returns nothing  */
        }
        public boolean createStudent(Student student) throws SQLException {
            String insertStudent = "INSERT INTO students(registration_number,student_name,date_of_birth,date_of_admission,course,gender) VALUES (?,?,?,?,?,?) ";

            return true;
        }
        public static String searchForStudent(String query) throws SQLException, ClassNotFoundException {
            com.codesol.JDBC.DbUtil dbUtil = new com.codesol.JDBC.DbUtil();
            ResultSet sRes = dbUtil.readData(query);
            String res ="";
            while (sRes.next())
            {
                res ="Name: " + sRes.getString("student_name")+" Registration Number "+ sRes.getString("registration_number");
                System.out.println(res);
            }
            System.out.println();
            return res;
        }

        public static int deleteStudentRecord(String query) throws SQLException, ClassNotFoundException {
            com.codesol.JDBC.DbUtil dbUtil = new com.codesol.JDBC.DbUtil();
            int deleteRes = dbUtil.updateData(query);
            System.out.println(deleteRes);
            System.out.println("Student Deleted Successfully");
            return deleteRes;
        }

        public static void readAllStudents(DbUtil dbUtil) throws SQLException {
            ResultSet students = dbUtil.readData("select * from students");
            while (students.next())
            {
                System.out.println(" Student Name: "+ students.getString("student_name")+
                        " Registration Number: "+ students.getString("registration_number")+" Gender: "+ students.getString("gender"));
            }
            System.out.println();
        }

        public void closeConnection() throws SQLException {
            statement.close();
            connection.close();
        }
    }
