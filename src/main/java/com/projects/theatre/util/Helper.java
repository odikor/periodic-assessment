package com.projects.theatre.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Helper {
    public static  String dateToStringFormatter(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        return format.format(date);
    }
    public static Date stringToDateFormatter(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Date newDate= null;
        try {
            newDate = format.parse(date);
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        }
        return newDate;
    }
}
