package com.projects.theatre.util;
import java.sql.*;

public abstract class DbUtil {
    /**
     * Database connection requirements
     *  - Register connection
     *  - Open connection
     *  - Execute Query
     *  - CLose connection
     * */
    //1. Create Connection
    private final Connection connection;
    private Statement statement;
    String className = "com.mysql.cj.jdbc.Driver";
    String url = "jdbc:mysql://localhost:3306/eventsdb";
    String username = "root";
    String password = "Jenna019#";

    public DbUtil() throws ClassNotFoundException, SQLException {
        Class.forName(className);
        connection = DriverManager.getConnection(url, username, password);
    }
    @Override
    protected void finalize() throws Throwable {
        this.closeConnection();
    }
    //2. Read data from Database
    public ResultSet readData(String query) throws SQLException { /**@Returns a ResultSet - Takes in a query, Example is Select, search*/
        statement = connection.createStatement();
        return statement.executeQuery(query);
    }
    /**@Insert Values into the database*/
    public int updateData(String query) throws SQLException {
        statement = connection.createStatement();
        return statement.executeUpdate(query); /**@Check_out the class object executeupdate (Insert, update, Delete,Drop)
         *@Returns int or returns nothing  */
    }
    //Close Connection
    public void closeConnection() throws SQLException {
        statement.close();
        connection.close();
    }
}
