package com.projects.theatre.logic;

import com.projects.theatre.util.DbUtil;
import com.projects.theatre.model.Enum.Level;
import com.projects.theatre.model.Enum.Status;
import com.projects.theatre.model.Ticket;
import com.projects.theatre.util.Helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TicketLogic extends DbUtil implements CrudInterface<Ticket> {
    Helper helper = new Helper();

    public TicketLogic() throws ClassNotFoundException, SQLException {
    }

    @Override
    public boolean add(Ticket ticket) {
        int results = 0;
        try {
            results = updateData("insert into tbl_tickets (ticketNumber,ticketTitle,price,status,level,datePurchased,expiryDate) values('" + ticket.getTicketNumber() + "','" + ticket.getTicketTitle() + "','" + ticket.getPrice() + "','" + ticket.getStatus() + "','"
                    + ticket.getLevel()+"','"+helper.dateToStringFormatter(ticket.getDatePurchased())+ "','"+helper.dateToStringFormatter(ticket.getExpiryDate())+"')");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return results == 1;
    }

    @Override
    public boolean delete(Ticket ticket) throws SQLException {
        String query = "delete from tbl_tickets where ticketNumber='" + ticket.getTicketNumber() + "'";
        int results = updateData(query);
        return results == 1;
    }
    @Override
    public List<Ticket> read(Ticket ticket) throws SQLException {
        List<Ticket> tickets = new ArrayList<>();
        String query = "SELECT * FROM tbl_tickets";
        ResultSet resultSet = readData(query);
        while (resultSet.next()) {
            Ticket readTicket = convertToTicket(resultSet);
            tickets.add(readTicket);
        }
        return tickets;
    }

    @Override
    public List<Ticket> search(Ticket ticket) throws SQLException {
        List<Ticket> tickets = new ArrayList<>();
        String query = "SELECT * FROM tbl_tickets WHERE ticketNumber='"+ticket.getTicketNumber()+"'";
        ResultSet resultSet = readData(query);
        while (resultSet.next()) {
            Ticket searchedTicket =convertToTicket(resultSet);
            tickets.add(searchedTicket);
        }
        System.out.println(tickets);
        return tickets;
    }

    private Ticket convertToTicket(ResultSet resultSet) throws SQLException {
        Ticket ticket = new Ticket();
        ticket.setTicketNumber(resultSet.getString("ticketNumber"));
        ticket.setTicketTitle(resultSet.getString("ticketTitle"));
        ticket.setStatus(Status.valueOf(resultSet.getString("status")));
        ticket.setLevel(Level.valueOf(resultSet.getString("level")));
        ticket.setPrice(resultSet.getDouble("price"));
        ticket.setDatePurchased(resultSet.getDate("datePurchased"));
        ticket.setExpiryDate(resultSet.getDate("expiryDate"));

        return ticket;
    }
}
