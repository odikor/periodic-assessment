package com.projects.theatre.logic;

import java.sql.SQLException;
import java.util.List;

public interface CrudInterface <T>{
    boolean add(T t) throws SQLException;
    boolean delete(T t) throws SQLException;
    List<T> read(T t) throws SQLException;
    List<T> search(T t) throws SQLException;
}