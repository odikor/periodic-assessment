package com.projects.theatre.logic;

import com.projects.theatre.model.Enum.Available;
import com.projects.theatre.model.Event;
import com.projects.theatre.util.DbUtil;
import com.projects.theatre.util.Helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EventLogic extends DbUtil implements CrudInterface<Event>{
    List<Event> eventList = new ArrayList<>();
    Helper helper= new Helper();

    public EventLogic() throws ClassNotFoundException, SQLException {
        super();
    }

    @Override
    public boolean add(Event event) {
        // events = EventUi.getEvents();
        int results = 0;
        try {
            results = updateData("insert into tbl_events (eventNumber,eventName,eventLocation,startDate,duration,available) values('" + event.getEventNumber() + "','" + event.getEventName() + "','" + event.getEventLocation()
                    + "','" + helper.dateToStringFormatter(event.getStartDate())+ "','"+ event.getDuration() + "','" + event.getAvailable() + "')");
        } catch (SQLException e) {
            e.getMessage();
        }

        return results == 1;
    }

    @Override
    public boolean delete(Event event) throws SQLException {
        String query = "delete from tbl_events where eventNumber='" + event.getEventNumber() + "'";
        int results = updateData(query);
        return results == 1;
    }

    @Override
    public List<Event> read(Event event) throws SQLException {
        String query = "SELECT * FROM tbl_events";
        ResultSet resultSet = readData(query);
        while (resultSet.next()) {
            Event eventE = convertToEventObject(resultSet);
            eventList.add(eventE);
        }
        return eventList;
    }

    @Override
    public List<Event> search(Event event) throws SQLException {
        String query = "SELECT * FROM tbl_events WHERE eventNumber='" + event.getEventNumber() + "'";
        ResultSet resultSet = readData(query);
        while (resultSet.next()) {
            Event eventSearch = convertToEventObject(resultSet);
            eventList.add(eventSearch);
        }
        return eventList;
    }

    private Event convertToEventObject(ResultSet resultSet) throws SQLException {
        Event event = new Event();
        event.setEventNumber(resultSet.getString("eventNumber"));
        event.setEventName(resultSet.getString("eventName"));
        event.setEventLocation(resultSet.getString("eventLocation"));
        event.setStartDate(resultSet.getDate("startDate"));
        event.setDuration(resultSet.getInt("duration"));
        event.setAvailable(Available.valueOf(resultSet.getString("available")));

        return event;
    }

}
