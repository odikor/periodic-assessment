package com.projects.theatre.logic;

import com.codesol.JDBC.Enum.Gender.Gender;
import com.projects.theatre.util.DbUtil;
import com.projects.theatre.model.Client;
import com.projects.theatre.model.Enum.State;
import com.projects.theatre.model.Event;
import com.projects.theatre.model.Ticket;
import com.projects.theatre.util.Helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientLogic extends DbUtil implements CrudInterface<Client> {
    Helper helper = new Helper();

    public ClientLogic() throws ClassNotFoundException, SQLException {
        super();
    }

    //Client clients;

    @Override
    public boolean add(Client client) throws SQLException {
        int results = updateData("insert into tbl_clients (clientName,idNumber,gender,dateOfBirth,address,state,ticketNumber) values('"
                + client.getClientName() + "','" + client.getIdNumber() + "','" + client.getGender() + "','"+ helper.dateToStringFormatter(client.getDateOfBirth())+ "','" + client.getAddress() + "','"
                + client.getState() + "')");

        return results == 1;
    }

    @Override
    public boolean delete(Client client) throws SQLException {
        String query = "delete from tbl_clients where idNumber='" + client.getIdNumber() + "'";
        int results = updateData(query);
        return results == 1;
    }

    @Override
    public List<Client> read(Client client) throws SQLException {
        List<Client> clientList = new ArrayList<>();
        ResultSet resultSet = readData("select * from tbl_clients");
        while (resultSet.next()) {
            Client readClient = convertToClientObject(resultSet);

            clientList.add(readClient);
        }
        return clientList;
    }

    @Override
    public List<Client> search(Client client) throws SQLException {
        List<Client> clientList = new ArrayList<>();
        ResultSet resultSet = readData("select * from tbl_clients where idNumber ='"+client.getIdNumber()+"'");
        while (resultSet.next()) {
            Client newClient = convertToClientObject(resultSet);
            clientList.add(newClient);
        }
        return clientList;
    }

    private Client convertToClientObject(ResultSet resultSet) throws SQLException {
        Client client = new Client();
        client.setClientName(resultSet.getString("clientName"));
        client.setIdNumber(resultSet.getInt("idNumber"));
        client.setAddress(resultSet.getString("address"));
        client.setDateOfBirth(resultSet.getDate("dateOfBirth"));
        client.setGender(Gender.valueOf(resultSet.getString("gender")));
        client.setState(State.valueOf(resultSet.getString("state")));

       return client;
    }
    private boolean buyTicket( Event event, Client client) throws SQLException, ClassNotFoundException {
        Ticket ticket = new Ticket();
       TicketLogic ticketLogic = new TicketLogic();
        try {
            ticketLogic.read(ticket);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        List<Ticket> tickets = ticketLogic.search(ticket);
      //  tickets.set(dbUtil.updateData("update tbl_tickets set status='purchased' where ticketNumber ='"+ticket.getTicketNumber()+"'"),ticket.setStatus(Status.Purchased));
        return false;
    }
}
