package com.projects.theatre.tests;

import com.codesol.JDBC.Enum.Gender.Gender;
import com.projects.theatre.logic.ClientLogic;
import com.projects.theatre.logic.EventLogic;
import com.projects.theatre.logic.TicketLogic;
import com.projects.theatre.model.Client;
import com.projects.theatre.model.Enum.Available;
import com.projects.theatre.model.Enum.Level;
import com.projects.theatre.model.Enum.State;
import com.projects.theatre.model.Enum.Status;
import com.projects.theatre.model.Event;
import com.projects.theatre.model.Ticket;
import com.projects.theatre.util.Helper;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class TestTicketing {
    Helper helper = new Helper();
    List<String> expectedOutPut = new ArrayList<>();

    /**@Creation and insertion of values into Database*/
    @Test
    void testAddClient() throws SQLException, ParseException, ClassNotFoundException {
        Client client = new Client();
        client.setIdNumber(19091);
        client.setClientName("Musa Omuse");
        client.setAddress("Malakisi");
        client.setGender(Gender.Male);
        client.setDateOfBirth(helper.stringToDateFormatter("1998/05/23"));
        client.setState(State.Active);
        //client.setTicketNumber("AFD567");
        Assert.assertTrue(new ClientLogic().add(client));
    }

    @Test
    void testAddEvent() throws ParseException, SQLException, ClassNotFoundException {
        Event event = new Event();
        event.setEventName("Churchill Show");
        event.setEventNumber("CHS100");
        event.setEventLocation("Carnivore");
        event.setStartDate(helper.stringToDateFormatter("2021/09/01"));
        event.setDuration(7);
        event.setAvailable(Available.Yes);

        Assert.assertTrue(new EventLogic().add(event));
    }

    @Test
    void testAddTicket() throws SQLException, ClassNotFoundException {
        Ticket ticket = new Ticket();
        ticket.setTicketNumber("TK101");
        ticket.setTicketTitle("Laugh Festival");
        ticket.setLevel(Level.VVIP);
        ticket.setPrice(5000.00);
        ticket.setDatePurchased(helper.stringToDateFormatter("2021/08/21"));
        ticket.setExpiryDate(helper.stringToDateFormatter("2021/09/01"));
        ticket.setStatus(Status.Available);

        Assert.assertTrue(new TicketLogic().add(ticket));
    }
/**@Retreive From Database Test*/
    @Test
    void testSearchTicket()
    {
        Ticket ticket = new Ticket();
        ticket.setTicketNumber(String.valueOf("TK101"));
        expectedOutPut.add("TK101");
        expectedOutPut.add("Laugh Festival");
        expectedOutPut.add("Available");
        expectedOutPut.add("VVIP");
        expectedOutPut.add("5000.0");
        expectedOutPut.add("2021-08-21");
        expectedOutPut.add("2021-09-01");

        try {
            Assert.assertEquals(expectedOutPut.toString(),new TicketLogic().search(ticket).toString());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Test
    void testSearchEvent()
    {
        Event event = new Event();
        event.setEventNumber(String.valueOf("CHS100"));
        expectedOutPut.add("CHS100");
        expectedOutPut.add("Churchill Show");
        expectedOutPut.add("Carnivore");
        expectedOutPut.add("2021/09/01");
        expectedOutPut.add("7");
        expectedOutPut.add("Yes");
        try {
            Assert.assertEquals(expectedOutPut,new EventLogic().search(event));
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Test
    void testSearchClient()
    {
        Client client = new Client();
        client.setIdNumber(19090);
        expectedOutPut.add("Moses Omuse");
        expectedOutPut.add("19090");
        expectedOutPut.add("Male");
        expectedOutPut.add("1998-05-23");
        expectedOutPut.add("Malakisi");
        expectedOutPut.add("Active");
        expectedOutPut.add("AFD567");
        try {
            Assert.assertEquals(expectedOutPut.toString(),new ClientLogic().search(client).toString());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    /**@Test for deletion of records*/
    @Test
    void testDeleteTicket()
    {
        Ticket ticket = new Ticket();
        ticket.setTicketNumber("TK101");

        try {
            Assert.assertTrue(new TicketLogic().delete(ticket));
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Test
    void testDeleteEvent()
    {
        Event event = new Event();
        event.setEventNumber("TK101");

        try {
            Assert.assertTrue(new EventLogic().delete(event));
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Test
    void testDeleteClient()
    {
        Ticket ticket = new Ticket();
        ticket.setTicketNumber("");

        try {
            Assert.assertTrue(new TicketLogic().delete(ticket));
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
