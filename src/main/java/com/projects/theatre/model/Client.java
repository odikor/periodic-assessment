package com.projects.theatre.model;

import com.codesol.JDBC.Enum.Gender.Gender;
import com.projects.theatre.model.Enum.State;

import java.util.Date;
import java.util.List;
/** List of Clients */
public class Client {
    private String clientName;
    private Integer idNumber;
    private Gender gender;
    private Date dateOfBirth;
    private String address;
    private State state;

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Integer getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(Integer idNumber) {
        this.idNumber = idNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return clientName +
                ", " + idNumber +
                ", " + gender +
                ", " + dateOfBirth +
                ", " + address +
                ", " + state;
    }
}
