package com.projects.theatre.model;

import com.projects.theatre.model.Enum.Level;
import com.projects.theatre.model.Enum.Status;

import java.util.Date;
/** List of Tickets*/
public class Ticket {
    private String ticketNumber;
    private String ticketTitle;
    private Status status;
    private Level level;
    private double price;
    private Date datePurchased;
    private  Date expiryDate;
    /**
     * Match the number of tickets created with the maximum tickets for an event
     * @Ticket should be tied to an event
     *  and to a client*/

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getTicketTitle() {
        return ticketTitle;
    }

    public void setTicketTitle(String ticketTitle) {
        this.ticketTitle = ticketTitle;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDatePurchased() {
        return datePurchased;
    }

    public void setDatePurchased(Date datePurchased) {
        this.datePurchased = datePurchased;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public String toString() {
        return      ticketNumber +
                ", " + ticketTitle +
                ", "+ status +
                ", " + level +
                ", "+ price +
                ", " + datePurchased +
                ", " + expiryDate ;
    }
}
