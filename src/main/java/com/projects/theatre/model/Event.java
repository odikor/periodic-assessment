package com.projects.theatre.model;

import com.projects.theatre.model.Enum.Available;

import java.util.Date;
import java.util.List;
/** List of events*/
public class Event {
    private String eventNumber;
    private String eventName;
    private String eventLocation;
    private Date startDate;
    private int  duration;
    private Available available;

   // private Integer maxTickets;

    public String getEventNumber() {
        return eventNumber;
    }

    public void setEventNumber(String eventNumber) {
        this.eventNumber = eventNumber;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
    public Available getAvailable() {
        return available;
    }

    public void setAvailable(Available available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return  eventNumber + '\'' +
                ", " +eventName + '\'' +
                ", " +eventLocation + '\'' +
                ", " +startDate +'\''+
                ", " +duration +'\''+
                ", " +available;
    }
}
