package com.projects.theatre.ui;

import com.projects.theatre.model.Event;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class EventUi {
    public static Event getEvent() throws ParseException {
        Event events = new Event();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter event Number");
        events.setEventNumber(scanner.nextLine());
        System.out.println("Enter Event Name");
        events.setEventName(scanner.nextLine());
        System.out.println("Enter event Location/Address");
        events.setEventLocation(scanner.nextLine());
        System.out.println("Enter start date");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        events.setStartDate(dateFormat.parse(scanner.nextLine()));

        System.out.println("Enter Duration of event");
        events.setDuration(scanner.nextInt());

        return events;
    }
}
