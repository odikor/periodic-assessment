package com.projects.theatre.ui;

import com.codesol.JDBC.Enum.Gender.Gender;
import com.projects.theatre.model.Client;
import com.projects.theatre.model.Event;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class ClientUi {
    public static Client getClient() throws ParseException {
        Client client = new Client();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Client Name");
        client.setClientName(scanner.nextLine());
        System.out.println("Enter Client ID Number");
        client.setIdNumber(scanner.nextInt());
        System.out.println("Enter Client gender");
        client.setGender(Gender.valueOf(scanner.nextLine()));
        System.out.println("Enter Date of Birth");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        client.setDateOfBirth(dateFormat.parse(scanner.nextLine()));

        System.out.println("Enter Client address");
        client.setAddress(scanner.nextLine());

        return client;
    }
}
