package com.projects.theatre.ui;

import com.mysql.cj.x.protobuf.MysqlxDatatypes;
import com.projects.theatre.model.Enum.Level;
import com.projects.theatre.model.Enum.Status;
import com.projects.theatre.model.Ticket;

import java.util.Scanner;

public class TicketUi {
    public static Ticket getTicket()
    {
        Ticket ticket = new Ticket();
        Scanner scanner= new Scanner(System.in);
        System.out.println("Enter Ticket Number");
        ticket.setTicketNumber(scanner.nextLine());
        System.out.println("Enter Ticket Title");
        ticket.setTicketTitle(scanner.nextLine());
        System.out.println("Enter status of ticket");
        ticket.setStatus(Status.valueOf(scanner.nextLine()));
        System.out.println("Enter ticket class/level");
        ticket.setLevel(Level.valueOf(scanner.nextLine()));

       // if (ticket.getLevel()==ticket.setLevel(Level.Regular))
        System.out.println("Enter Ticket Price");
        ticket.setPrice(scanner.nextDouble());

        return ticket;
    }
}
