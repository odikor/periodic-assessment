package com.codesol.Threads;

class Printer{
   synchronized void printDocument(int numOfCopies, String docName)
    {
        for (int i=1; i<=numOfCopies; i++)
        {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(">>Printing "+docName+ " "+ i);
        }
    }
}
class MyThread extends Thread{
    Printer pRef;
    MyThread(Printer p)
    {
        pRef = p;
    }
    @Override
    public void run()
    {
        pRef.printDocument(10,"JohnProfile.pdf");
    }
}
class YourThread extends Thread{
    Printer pRef;
    YourThread(Printer p)
    {
        pRef = p;
    }
    @Override
    public void run()
    {
        pRef.printDocument(10,"FionaProfile.pdf");
    }
}
public class SyncApp {
    public static void main(String[] args) {
        System.out.println("====Application Started====");
            Printer printer = new Printer();
            printer.printDocument(10,"Resume");
            //int i = 10/0;
        //Scenario is that we have multiple threads working on the same pronter object
        MyThread mRef = new MyThread(printer); // having reference to the printer object
        YourThread yRef = new YourThread(printer);
        mRef.start();
        /*try {
            mRef.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        yRef.start();
        System.out.println("===Application Finished===");
    }
}
