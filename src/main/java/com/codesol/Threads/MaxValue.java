package com.codesol.Threads;

import java.util.Random;

public class MaxValue extends Thread{

    static final int MAX_THREADS = 4; //The maximum number of threads
    static final int ARR_LENGTH = 4; //Get the array Length
    static final int MAX_RANDOM_NUMBER = 1000; // Maximum possible random number

    private int low; //Low value index in the array
    private int high; //High value index in the array
    private int[] values; //Array of the values to check
    private int maxValue = 0; //Result of the maximum Value

    public MaxValue(int[] arrayValues, int arrayLow, int arrayHigh) { // The contractor taking in the value

        this.low = arrayLow;
        this.high = arrayHigh;
        this.values = arrayValues;

    }

    public static void main(String[] args) throws InterruptedException {

        int[] arr = new int[ARR_LENGTH];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = getRandomNumber();
            System.out.print(arr[i]+" ");
        }
        System.out.println();

        int maxNumberInArray = getMaxValue(arr);

        System.out.println(String.format("The max value is: %1$d", maxNumberInArray));

    }

    @Override
    public void run() {

        try {
            maxValue = getMaxValue(values, low, high);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }
    private static int getMaxValue(int[] numberArray){

        int maxValue = 0;
        int length = numberArray.length;

        MaxValue[] maxValueThreadArray = new MaxValue[MAX_THREADS];
        int [] finalValues = new int [MAX_THREADS];

        for (int k = 0; k < MAX_THREADS; k++) {

            int lowValue = (k * length) / MAX_THREADS;
            int highValue = ((k + 1) * length / MAX_THREADS);

            maxValueThreadArray[k] = new MaxValue(numberArray, lowValue, highValue);
            maxValueThreadArray[k].start();
        }

        for (int i = 0; i < MAX_THREADS; i++) {
            try {
                maxValueThreadArray[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            finalValues[i] = maxValueThreadArray[i].maxValue;
        }

        maxValue = getMaxValue(finalValues,0,MAX_THREADS);

        return maxValue;
    }


    static int getMaxValue(int[] pValues, int pLow, int pHigh) {

        int maxValue = 0;

        for (int i = pLow; i < pHigh; i++)
        {
            if (pValues[i] > maxValue)
            {
                maxValue = pValues[i];
            }
        }
        return maxValue;

    }

    /**
     ** Gets a random number
     ** return the random number
     **/
    private static int getRandomNumber(){

        Random random = new Random();
        return random.nextInt(MAX_RANDOM_NUMBER);

    }
}
