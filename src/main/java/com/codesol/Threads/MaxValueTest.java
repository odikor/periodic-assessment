package com.codesol.Threads;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class MaxValueTest {
    @Test
    void getMaxTest()
    {
        int[] arrVal ={123,900,90,359};
        int plow = 0;
        int pHigh = 4;

        Assert.assertEquals(900, new MaxValue(arrVal,plow,pHigh).getMaxValue(arrVal,plow,pHigh));
    }

}