package com.codesol.Threads;
class newDomo implements Runnable {
    @Override
    public void run()
    {
        int[] anArray;	        // declare an array of integers
        anArray = new int[10];	// create an array of integers
        // assign a value to each array element and print
        for (int i = 0; i < anArray.length; i++) {
            anArray[i] = i;
            System.out.print(anArray[i] + " ");
        }
        System.out.println();
    }
}
public class ThreadDemo implements Runnable{ //Child thread/Worker thread
    @Override
    public void run() {
        for (int doc=0; doc<12; doc++)
        System.out.println("Printing "+doc+" - Printer 2");
    }
    public static void main(String[] args) { //main represents the main thread
        //Threads always executes jobs in a sequence
        //Thread is an execution context
        //Both the main method and the thread are executing in parallel/concurrently
        System.out.println("==Application staring==");
        Thread t = new Thread(new ThreadDemo());
        t.start();
        for (int doc=0; doc<12; doc++)
            System.out.println("Fax document "+doc+" - Printer 1");
        new Thread(new newDomo()).start();
        System.out.println("===Application Finished====");
    }
}
