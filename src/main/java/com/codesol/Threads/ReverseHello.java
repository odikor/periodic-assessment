package com.codesol.Threads;

public class ReverseHello extends Thread{
    int counter;
    public ReverseHello(int counter){
            this.counter=counter;
        }
    public void run()
    {
     //System.out.println("Starting Thread: "+ counter);
     if(counter<50)
     {
       CreateThreads newThread = new CreateThreads();
       newThread.createThread(counter+1);
     }
     System.out.println("Hello from Thread <" + counter+"> ");
     }
    private static void executeReverse() {
        ReverseHello thread = new ReverseHello(1);
        thread.start();
    }
    public static void main(String[] arg){
        executeReverse();
    }
}