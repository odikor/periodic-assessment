package com.codesol.Threads;

public class CounterShare extends Thread{
    private int counter = 0;

    public static void main(String[] args) throws InterruptedException {
        CounterShare threads = new CounterShare();

        Thread thread1 = new Thread(() -> threads.increment());
        thread1.start();
      //  Thread.yield();
        Thread thread2 = new Thread(() -> threads.increment());
        thread2.start();
        Thread thread3 = new Thread(() -> threads.increment());
        thread3.start();
        Thread thread4 = new Thread(() -> threads.increment());
        thread4.start();
        Thread thread5 = new Thread(() -> threads.increment());
        thread5.start();
        Thread thread6 = new Thread(() -> threads.increment());
        thread6.start();
        Thread thread7 = new Thread(() -> threads.increment());
        thread7.start();
        Thread thread8 = new Thread(() -> threads.increment());
        thread8.start();
        Thread thread9 = new Thread(() -> threads.increment());

        thread9.start();
        Thread thread10 = new Thread(() -> threads.increment());
        thread10.start();

        thread1.join();
        thread2.join();
        thread3.join();
        thread4.join();
        thread5.join();
        thread6.join();
        thread7.join();

        thread8.join();
        thread9.join();
        thread10.join();
        System.out.println("Counter: " + threads.counter);

    }
    public void increment() {
        for (int i = 0; i < 10; i++) {
         //  synchronized (CounterShare.class) {
                counter++;
          //  }
        }
    }
}
