package com.codesol.CatII;

public class Recursion {
    /**
     * @Param text catcowcat
     * @Param key cat
     * */

    public int strCount(String text, String key)
    {
        //Base condition (Exit condition)
        if(!text.contains(key)||text.isEmpty())
            return 0;

        //Actual work to be done
        int keyLength = key.length();
        String firstKeyLengthString = text.substring(0,keyLength);
        int keyCount =0;

        System.out.println(firstKeyLengthString +": "+ key);
        if(firstKeyLengthString.equals(key))
        {
            keyCount++;
            text = text.substring(keyLength);
        }
        else
            text = text.substring(1);//reduce the text by one character
        return keyCount + strCount(text, key);//Call method again and pass different parameters
    }
    public int advanceStrCount (String text, String key)
    {
            if(!text.contains(key) || text.isEmpty())     /**
             * @Conditions: Base condition/Exit condition
             * */
            return 0;
        if(text.startsWith(key))
        {
          return 1 + advanceStrCount(text.substring(key.length()),key);
        }
        else
            return advanceStrCount(text.substring(1), key);
    }
    public String pairStar(String text)
    {
        if(text.length()<=1)
            return text;

        if (text.charAt(0)==text.charAt(1))
            return text.charAt(0) + "*" + pairStar(text.substring(1));
        else
            return text.charAt(0) + pairStar(text.substring(1));
    }

    /**
     * @Question Three:
     * Given a string, compute recursively (no loops) the number of lowercase 'x' chars in the string.
     * For example:
     * countX("xxhixx") → 4
     * countX("xhixhix") → 3
     * countX("hi") → 0
     * *
     * */
    public int countX(String text)
    {
        if (text.length() <= 0)
            return 0;
        int count =(text.charAt(0)=='x') ? 1 : 0;
        if(text.length()==1)
        {
            return count;
        }
        return count + countX(text.substring(1));
    }
    /**
     * @Question Four:
     * We have triangle made of blocks. The topmost row has 1 block, the next row down has 2 blocks, the next row has 3 blocks, and so on.
     * Compute recursively (no loops or multiplication) the total number of blocks in such a triangle with the given number of rows.
     * For example:
     * triangle(0) → 0
     * triangle(1) → 1
     * triangle(2) → 3
     * */
    public int countTriangles(int rows)
    {
        if(rows<=0) {
            return 0;
        }
        return rows + countTriangles(rows - 1);
    }
    /**
     * @Question Five:
     * Given a string that contains a single pair of parenthesis, compute recursively a new string made of only of the parenthesis and their contents, so "xyz(abc)123" yields "(abc)".
     * For example:
     * parenBit("xyz(abc)123") → "(abc)"
     * parenBit("x(hello)") → "(hello)"
     * parenBit("(xy)1") → "(xy)"
     * */
    public static String parenthensis(String text)
    {
        if (text.length()<2)
            return " ";
        if (text.charAt(0) == '(') {
            int i = 1;
            boolean foundClosing = false;

            while (!foundClosing && (i < text.length())) {
                char letter = text.charAt(i);

                if (letter == ')') {
                    foundClosing = true;
                }
                i++;
            }
            return text.substring(0, i);
        }
        return parenthensis(text.substring(1));
    }

    public static String advancedParenthesis(String text) {
        if (text.charAt(0) != '(') {
            return advancedParenthesis(text.substring(1));
        }
        if (text.charAt(text.length() - 1) != ')') {
            return advancedParenthesis(text.substring(0, text.length() - 1));
        }
        return text;
    }
}
