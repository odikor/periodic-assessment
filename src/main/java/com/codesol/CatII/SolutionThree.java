package com.codesol.CatII;

import com.codesol.Personal.SortingArrays;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SolutionThree {
    public static int findMissing(int arr[], int brr[])
    {
        SortingArrays sortingArrays = new SortingArrays();
        sortingArrays.selectionSort(arr);
        sortingArrays.selectionSort(brr);
        int n = arr.length, m = brr.length;
        int mis = 0;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
                if (arr[i] == brr[j]) {
                    mis = arr[i];
                    if (j == m) {
                        System.out.print(arr[i] + " ");
                    }
                }
        }
    return mis;
    }
    /*public static List<Integer> compareArray(int[] arr, int[] brr)
    {
        SortingArrays sortingArrays= new SortingArrays();
        sortingArrays.selectionSort(arr);
        sortingArrays.selectionSort(brr);

        List<Integer> missingValues = new ArrayList<>();

        int n = arr.length;
        int m = brr.length;
        int missing = 0;
        if(n==m)
        { //bring in sort
            System.out.println("Have the same size, Compare values");
            for (int i=0; i<n;i++)
            {
                if(arr[i]==brr[i])
                {
                    System.out.println("Arrays are Equal");
                }
                if (arr[i]!= brr[i])
                {
                    missing = brr[i];
                    missingValues.add(missing);
                }
            }
            return missingValues;
        }
        else
        {
            System.out.println("Array different in size");
            int temp=0;
            for(int i=0;i< n;i++)
                {
                    for (int k=0;k<m;k++) {
                        if (arr[i] != brr[k]) {
                           missingValues.add(arr[i]);
                            //System.out.println(temp);
                        }
                    }
                }
            return missingValues;
        }
    }*/
    public static void printArray(int arr[])
    {
        int n = arr.length;
        for (int i = 0; i < n; ++i)
            System.out.print(arr[i] + " ");

        System.out.println();
    }
    /**
     * @Generate_random_Array to sort
     * */
    public static int[] generateArray(int arr[])
    {
        for (int k = 0; k < arr.length; k ++)
        {
            int value = new Random().nextInt(1000);
            arr[k] = value;
        }
        return arr;
    }
    public static void main(String[] args)
    {
        int a[] = new int[10];
        int b[] = new  int[15];
        a=generateArray(a);
        System.out.println("First Array: " );
        printArray(a);
        System.out.println();
        b=generateArray(b);
        System.out.println("Second array: ");
        printArray(b);

        System.out.println("Missing: " );
        findMissing(a, b);
    }
}
