package com.codesol.CatII;

import java.util.Arrays;
import java.util.Comparator;

public class SolutionTwo {
    public static void main(String[] args) {
        String[] arr ={"1", "2" ,"100" ,"12303479849857341718340192371", "3084193741082937", "3084193741082938" , "111" ,"200"};
        solve(arr);
    }

    public static String[] solve(String[] arr) {

        Comparator comparator = (Comparator<String>) (string1, string2) -> {
            if (string1.length() < string2.length()) {
                return -1;
            }
            if (string1.length() > string2.length()) {
                return 1;
            }
            return string1.compareTo(string2);
        };

        Arrays.sort(arr, comparator);
        for (String string : arr) {
            System.out.println(string);
        }
        return arr;
    }
}
