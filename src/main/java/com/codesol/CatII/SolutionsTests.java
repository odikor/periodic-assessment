package com.codesol.CatII;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.*;

public class SolutionsTests {
    @Test
    void SolutionOneTest() {
        int n = 148;
        int k = 3;
        Assert.assertEquals(3, new SuperDigits().getSuperDigit(n, k));
    }
    @Test
    void SolutionTwoTest() {
        String[] arr ={"1", "2" ,"100" ,"12303479849857341718340192371", "3084193741082937", "3084193741082938" , "111" ,"200"};

        Assert.assertEquals("[1, 2, 100, 111, 200, 3084193741082937, 3084193741082938, 12303479849857341718340192371]",
                            Arrays.toString(new SolutionTwo().solve(arr)));
    }
    @Test
    void solutionThreeTest()
    {
        int[] arr = new int[]{7,2,5,3,5,3};
        int[] brr = new int[]{7,2,5,4,6,3,5,3,45,78};
        List<Integer> res = new ArrayList<>();
        res.add(4);
        res.add(6);
        res.add(45);
        res.add(78);

         Assert.assertEquals(res, new QuestionThree().solve(arr,brr));
    }
     @Test
    void missingNumbersTest()
     {
         //Map<Integer,Integer> missing = new HashMap<>();
         int[] arr1 = new int[]{23,56,34,32,0,3};
         int[] arr2 = new int[]{23,56,34,32,9,3,0,16,3,32};

         int[] arr = {203,204,205,206,207,208,203,204,205,203,206};
         int[] brr = {203,204,205,206,207,208,203,204,205,203,206,204,208,205,206};


         Assert.assertEquals("[32, 16, 3, 9]", Arrays.toString(new QuestionThree().missingNumbers(arr1,arr2)));
         Assert.assertEquals("[208, 204, 205, 206]", Arrays.toString(new QuestionThree().missingNumbers(arr,brr)));
     }
     @Test
    public void testStrCount()
     {
         Assert.assertEquals(2,new Recursion().strCount("catcowcat","cat"));
         Assert.assertEquals(1,new Recursion().strCount("catcowcat","cow"));
         Assert.assertEquals(0,new Recursion().strCount("catcowcat","dog"));
     }
    @Test
    public void testAdvancedStrCount()
    {
        Assert.assertEquals(2,new Recursion().advanceStrCount("catcowcat","cat"));
        Assert.assertEquals(1,new Recursion().advanceStrCount("catcowcat","cow"));
        Assert.assertEquals(0,new Recursion().advanceStrCount("catcowcat","dog"));
    }
    @Test
    public void testPairStar()
    {
        Assert.assertEquals("hel*lo",new Recursion().pairStar("hello"));
        Assert.assertEquals("x*xyx*x",new Recursion().pairStar("xxyxx"));
        Assert.assertEquals("a*a*a*a",new Recursion().pairStar("aaaa"));
    }
    @Test
    public void testCountX()
    {
        Assert.assertEquals(4,new Recursion().countX("xxhixx"));
        Assert.assertEquals(3,new Recursion().countX("xhixhix"));
        Assert.assertEquals(0,new Recursion().countX("hi"));
        Assert.assertEquals(1,new Recursion().countX("xray"));
    }

    @Test
    public void testParenthesis() {
        String expected = "(abc)";
        Assert.assertEquals(expected, new Recursion().parenthensis("xyz(abc)123"));

        expected = "(hello)";
        Assert.assertEquals(expected, new Recursion().parenthensis("x(hello)"));

        expected = "(xy)";
        Assert.assertEquals(expected, new Recursion().parenthensis("(xy)1"));


        expected = "()";
        Assert.assertEquals(expected, new Recursion().parenthensis("()"));
    }
    @Test
    public void testCountTriangles()
    {
        Assert.assertEquals(0, new Recursion().countTriangles(0));
        Assert.assertEquals(1, new Recursion().countTriangles(1));
        Assert.assertEquals(3, new Recursion().countTriangles(2));
        Assert.assertEquals(6, new Recursion().countTriangles(3));
        Assert.assertEquals(10, new Recursion().countTriangles(4));

    }
    @Test
    public void testParenths() {
        String expected = "(abc)";
        Assert.assertEquals(expected, new Recursion().advancedParenthesis("xyz(abc)123"));

        expected = "(hello)";
        Assert.assertEquals(expected, new Recursion().advancedParenthesis("x(hello)"));

        expected = "(xy)";
        Assert.assertEquals(expected, new Recursion().advancedParenthesis("(xy)1"));


        expected = "()";
        Assert.assertEquals(expected, new Recursion().advancedParenthesis("()"));
    }

}
