package com.codesol.CatII;

import java.util.*;

public class QuestionThree {

    public static List<Integer> solve(int[] arr, int[] brr)
    {
        List<Integer> list = new ArrayList<>();
            for (int n:brr) {
                    if (!isPresent(n, arr)) {
                        for (int i = 0; i<arr.length; i++) {
                            list.add(n);
                            break;
                        }
                    }
            }
        System.out.println(list);
        return list;
    }
    private static boolean isPresent(int a, int[] arr)
    {
            for (int i : arr)
                if (a == i) {
                    return true;
                }
        return false;
    }

    public static void main(String[] args) {
        int[] arr1 = new int[]{23,56,34,32,0,3};
        int[] arr2 = new int[]{23,56,34,32,9,3,0,16,3,32};
        solve(arr1,arr2);
        System.out.println(isPresent(34,arr1));
        missingNumbers(arr1,arr2);
    }
    public static int[] missingNumbers(int[] arr, int[] brr) {

        HashMap<Integer, Integer> integerFreqMap = new HashMap<>();

        // Add elements of original list
        for (int i : brr) {
            int freq = integerFreqMap.getOrDefault(i, 0);
            freq++;
            integerFreqMap.put(i, freq);
        }
        System.out.println(integerFreqMap);
        // Remove elements of new list
        for (int i : arr) {
            System.out.println("Value i: "+i);
            int freq = integerFreqMap.get(i);
            freq--;
            if (freq == 0)
                integerFreqMap.remove(i);
            else
                integerFreqMap.put(i, freq);
        }
        System.out.println(integerFreqMap);

        // Create the result array
        int[] result = new int[integerFreqMap.size()];
        int i = 0;
        for (Map.Entry<Integer, Integer> integerIntegerEntry : integerFreqMap.entrySet()) {
            result[i++] = integerIntegerEntry.getKey();
        }
        System.out.println(Arrays.toString(result));
        return result;
    }

}
