package com.codesol.CatII;

public class SuperDigits {

        public static void main(String[] args) {

        int n = 148;
        int k = 3;

        getSuperDigit(n, k);
    }
        public static int getSuperDigit(int n, int k) {
        int i = concatenateNumber(n, k);
        return superDigit(i);
    }
        private static int concatenateNumber(int k, int n) {
        StringBuilder str = new StringBuilder(k);
        for(int i = 0; i < n; i++) {
            str.append(k);
        }
       // System.out.println( Integer.parseInt(String.valueOf(str)));
        return Integer.parseInt(String.valueOf(str));
    }
        public static Integer superDigit(Integer x) {
        if(x < 10) {
            return x;
        } else {
            int digitSum = 0;
            for (int num : x.toString().toCharArray()) {
                digitSum += Character.getNumericValue(num);
            }
            System.out.println(digitSum);
            return superDigit(digitSum);
        }
    }
}
