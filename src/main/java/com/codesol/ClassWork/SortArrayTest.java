package com.codesol.ClassWork;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class SortArrayTest {

    @Test
    void testSortArrays() {
        int arr[]= new int[10];
        int arr1[]= new int[10]; // Copy of the first array
        for (int k=0; k<arr.length; k++) {
            int values = new Random().nextInt(1000);
            arr[k] = values; //To offer the expected result
            arr1[k] = values; //To be tested
        }
        Arrays.sort(arr); //array arr is already sorted at this point
        Assert.assertEquals(Arrays.toString(arr), Arrays.toString(new SortArray().sortArrays(arr1)));
    }
}