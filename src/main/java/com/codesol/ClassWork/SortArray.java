package com.codesol.ClassWork;

import java.util.Arrays;
import java.util.Random;

public class SortArray {
    public static int[] sortArrays(int[] arr)
    {
        int length = arr.length;
        long startTime = System.nanoTime();
        // Sorting
        for (int j = 0; j < length - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
                j = -1;
            }
        }
        long endTime = System.nanoTime();
        System.out.println("Time taken :"+ (endTime-startTime));
        return arr;
    }
    public static void main(String args[])
    {
        int[] arr = generateArray();
        System.out.println("Original array: "+ Arrays.toString(arr));
        arr = sortArrays(arr);// Sorting the array with the sort array method
        System.out.println("Sorted array: "+ Arrays.toString(arr));
    }
    private static int[] generateArray() {
        //A randomly generated array
        int arr[] = new int[10];

        for (int k = 0; k < arr.length; k ++)
        {
            int value = new Random().nextInt(1000);
            arr[k] = value;
        }
        return arr;
    }
}
