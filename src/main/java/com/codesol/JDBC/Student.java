package com.codesol.JDBC;

import com.codesol.JDBC.Enum.Gender.Gender;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Student {
    private String registrationNumber;
    private String studentName;
    private Date dateOfBirth;
    private Date dateOfAdmission;
    private String course;
    private Gender gender;

    public Student(String registrationNumber, String name, Date dateOfBirth, Date dateOfAdmission, String course, Gender gender) {
        this.registrationNumber = registrationNumber;
        this.studentName = name;
        this.dateOfBirth = dateOfBirth;
        this.dateOfAdmission = dateOfAdmission;
        this.course = course;
        this.gender = gender;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Date getDateOfAdmission() {
        return dateOfAdmission;
    }

    public void setDateOfAdmission(Date dateOfAdmission) {
        this.dateOfAdmission = dateOfAdmission;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Student{" +
                "registrationNumber='" + registrationNumber + '\'' +
                ", name='" + studentName + '\'' +
                ", dateOfBirth=" + new SimpleDateFormat("yyyy/MM/dd").format(dateOfBirth) +
                ", dateOfAdmission=" + new SimpleDateFormat("yyyy/MM/dd").format(dateOfAdmission) +
                ", course='" + course + '\'' +
                ", gender=" + gender +
                '}';
    }
}
