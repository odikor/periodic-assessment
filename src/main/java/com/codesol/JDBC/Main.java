package com.codesol.JDBC;

import com.codesol.JDBC.Enum.Gender.Gender;
import org.w3c.dom.ranges.Range;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws SQLException, ClassNotFoundException, ParseException {
        displayMenu();
/*    Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Integer to Check: ");
        int N = scanner.nextInt();

        if((N % 2) !=0 ) // Check if Odd
        {
            System.out.println("weird");
        }
            if((N%2==0)) // Check if even
            {
                if (N<5 || N>20)
                {
                    System.out.println("not weird");
                }
                else if(N>=6 && N<=20)
                {
                    System.out.println("weird") ;
                }
            }
        scanner.close();*/
        }

    private static void displayMenu() throws SQLException, ClassNotFoundException, ParseException {
        DbUtil dbUtil = new DbUtil();
        Scanner scanner;
        int choice;
        System.out.println("Systech Cohort 9 Students");
        do {
            System.out.println("" +
                    "1. Add new Student to Students Table\n" +
                    "2. View Students in Students Table\n" +
                    "3. Search in Students Table\n" +
                    "4. Delete From Students Table\n" +
                    "5. Read Record From users Table\n" +
                    "0. Exit\n");
            scanner = new Scanner(System.in);
            choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 1:
                     getStudentDetails(scanner);
                    break;
                case 2:
                    dbUtil.readAllStudents(dbUtil);
                    break;
                case 3:
                    System.out.println("Enter student Registration number: ");
                    String regNo = scanner.nextLine();
                    dbUtil.searchForStudent("SELECT * FROM students WHERE registration_number = " + regNo +"");
                    break;
                case 4:
                    System.out.println("Enter student Registration number: ");
                    regNo = scanner.nextLine();
                   dbUtil.deleteStudentRecord("DELETE  FROM students WHERE registration_number = " + regNo + "");
                    break;
                case 5:
                    jdbcDemo(dbUtil);
                    break;
            }
        } while (choice != 0);
        dbUtil.closeConnection();

    }
    public static Student getStudentDetails(Scanner scanner) throws ParseException, SQLException, ClassNotFoundException {
        DbUtil dbUtil = new DbUtil();
        System.out.println("Enter Student Details:");
        System.out.println("Enter your Reg No: ");
        String regNo = scanner.nextLine();
        System.out.println("Enter your Name: ");
        String name = scanner.nextLine();
        System.out.println("Enter DOB: ");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String dob = scanner.nextLine();
        Date dateOfBirth = simpleDateFormat.parse(dob);

        System.out.println("Date of admission: ");
        String doa = scanner.nextLine();
        Date dateOfAdmission = simpleDateFormat.parse(doa);

        System.out.println("Enter Course: ");
        String course = scanner.nextLine();

        System.out.println("Enter Gender: ");
        String gender = scanner.nextLine();
        Gender gen = Gender.valueOf(gender);

        dbUtil.createStudent(new Student(regNo,name,dateOfBirth,dateOfAdmission,course,gen));
        return new Student(regNo,name,dateOfBirth,dateOfAdmission,course,gen);
    }
    private static void jdbcDemo(DbUtil dbUtil) throws SQLException {
        ResultSet rs = dbUtil.readData("select * from users");
        while (rs.next())
        {
            System.out.println("ID: "+rs.getString("id") +", " + rs.getString("first_name")
                    +" "+ rs.getString("last_name")+", "+ rs.getString("email"));
        }
        int res = dbUtil.updateData("Insert INTO users(id) values(null);");
        System.out.println("Result set is "+ res);
    }

    /*private static void insertStudent(DbUtil dbUtil) throws SQLException {
        int insertStudent = dbUtil.updateData("INSERT INTO students(id) VALUES(null);");
        System.out.println("Result set is "+ insertStudent);
    }*/
}
