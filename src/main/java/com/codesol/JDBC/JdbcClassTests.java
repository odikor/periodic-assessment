package com.codesol.JDBC;

import com.codesol.JDBC.Enum.Gender.Gender;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class JdbcClassTests {

    SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
    @Test
    void saveStudentTest() throws ParseException, SQLException, ClassNotFoundException {
    Student student = new Student("1000","Mikey Angelo",format.parse("2009/09/28"),format.parse("2019/09/28"),"Java SE", Gender.valueOf("Male"));
    Assert.assertTrue(new DbUtil().createStudent(student));
    }
    @Test
    void searchStudentTest() throws SQLException, ClassNotFoundException {
    String regNo = "1002";
    String result ="Name: Mikey Angelo Registration Number 1002";
        String query = "SELECT * FROM students WHERE registration_number = " + regNo + "";
        Assert.assertEquals(result, new DbUtil().searchForStudent(query));
    }
    @Test
    void deleteStudentTest() throws SQLException, ClassNotFoundException {
        String regNo = "1000";
        String query ="DELETE  FROM students WHERE registration_number = " + regNo + "";
     Assert.assertEquals(1,new DbUtil().deleteStudentRecord(query));
    }
}
