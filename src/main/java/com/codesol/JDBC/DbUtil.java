package com.codesol.JDBC;

import com.codesol.JDBC.Enum.Gender.Gender;

import java.sql.*;
import java.text.SimpleDateFormat;

public class DbUtil {
    /**
     *@Database connection requirements
     *  - Register connection
     *  - Open connection
     *  - Execute Query
     *  - CLose connection
     * */
    //1. Create Connection
    private final Connection connection;
    private Statement statement;
        String className = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3305/javasedemo";
        String root = "root";
        String password = "";

    public DbUtil() throws ClassNotFoundException, SQLException {
        Class.forName(className);
        connection = DriverManager.getConnection(url, root, password);
    }

    @Override
    protected void finalize() throws Throwable {
        this.closeConnection();
    }

    //2. Read data from Database
    public ResultSet readData(String query) throws SQLException { /**@Returns a ResultSet - Takes in a query, Example is Select, search*/
        statement = connection.createStatement();
       return statement.executeQuery(query);
    }
    /**@Insert Values into the database*/
    public int updateData(String query) throws SQLException {
        statement = connection.createStatement();
        return statement.executeUpdate(query); /**@Check_out the class object executeupdate (Insert, update, Delete,Drop)
                                                *@Returns int or returns nothing  */
    }
    public boolean createStudent(Student student) throws SQLException {
        String insertStudent = "INSERT INTO students(registration_number,student_name,date_of_birth,date_of_admission,course,gender) VALUES (?,?,?,?,?,?) ";

        String regNO = student.getRegistrationNumber();
        String studentName = student.getStudentName();
        String dob=new SimpleDateFormat("yyyy/MM/dd").format(student.getDateOfBirth());
        String doa=new SimpleDateFormat("yyyy/MM/dd").format(student.getDateOfAdmission());
        String course = student.getCourse();
        Gender gender = student.getGender();

        PreparedStatement statement = connection.prepareStatement(insertStudent);
        statement.setString(1,regNO);
        statement.setString(2,studentName);
        statement.setString(3, dob);
        statement.setString(4, doa);
        statement.setString(5,course);
        statement.setString(6, String.valueOf(gender));

        System.out.println(statement.executeUpdate());
        //return statement.executeUpdate();
        return true;
    }
    public static String searchForStudent(String query) throws SQLException, ClassNotFoundException {
        DbUtil dbUtil = new DbUtil();
        ResultSet sRes = dbUtil.readData(query);
        String res ="";
        while (sRes.next())
        {
             res ="Name: " + sRes.getString("student_name")+" Registration Number "+ sRes.getString("registration_number");
            System.out.println(res);
        }
        System.out.println();
      return res;
    }

    public static int deleteStudentRecord(String query) throws SQLException, ClassNotFoundException {
        DbUtil dbUtil = new DbUtil();
        int deleteRes = dbUtil.updateData(query);
        System.out.println(deleteRes);
        System.out.println("Student Deleted Successfully");
        return deleteRes;
    }

    public static void readAllStudents(DbUtil dbUtil) throws SQLException {
        ResultSet students = dbUtil.readData("select * from students");
        while (students.next())
        {
            System.out.println(" Student Name: "+ students.getString("student_name")+
                    " Registration Number: "+ students.getString("registration_number")+" Gender: "+ students.getString("gender"));
        }
        System.out.println();
    }

    public void closeConnection() throws SQLException {
        statement.close();
        connection.close();
    }
}
