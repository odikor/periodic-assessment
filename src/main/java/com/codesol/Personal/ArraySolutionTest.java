package com.codesol.Personal;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class ArraySolutionTest {
    @Test
    public void printPairTest() {
        /**
         * Declare the static values to use while Testing the printPairs().
         * */
        int[] array = {11, 45, 39, 27};
        int sum = 72;

        Assert.assertNotEquals("", new arraySolution().printPairs(array, sum));
        Assert.assertEquals("(1,3)", new arraySolution().printPairs(array, sum));
        /**
         * The test above asserts that the value is not equal to the given value
         * & the second asserts that the value should be equal to the expected
         * */
    }
}
