package com.codesol.Personal;

import java.util.Comparator;

class Checker implements Comparator<Player> {
    @Override
    public int compare(Player o1, Player o2) {
        if (o1.score == o2.score)
        {
            return o1.name.compareToIgnoreCase(o2.name); //Returns the players in alphabetical order
        }
        return Integer.compare(o2.score,o1.score); // compares by score
    }
}
