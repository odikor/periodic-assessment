package com.codesol.Personal;

import java.util.Random;

public class Main {
public static int a[];
    public static void main(String[] args) {
	/*// write your code here
       forTenK(); //576,939,800 //405,900
       forHundredK();//4,379,170,800 //2,875,000
       forMillion(); //21,112,034,500 //4,544,600
       forTenMillion();//197,737,540,300 // 4,562,300
        System.out.println();
        System.out.println("Rate if time difference: \n" +
                "Hundred Thousand: "+forHundredK()/forTenK()+"% \n"+
                "A million : "+forMillion()/forHundredK()+"% \n"+
                "Ten Million: "+forTenMillion()/forMillion()+"%");*/
       /* SortArray sortArray= new SortArray();
        */
        /*int a[] = new Random().nextInt(1000);*/
        /*

        int[] a = new int[0];
        sortArray.createArray(a);*/
        /*createArray();
        sortArray();*/
        /*long factorial = factorialWithRecursion(9);
        System.out.println(factorial);*/
        /*SortingArrays sort = new SortingArrays();
        sort.generateArray(a);
        sort.printArray(a);*/

    }
    public static long factorialWithRecursion(int number)
    {
        if(number==0||number==1)
            return 1;
        return number * factorialWithRecursion(number-1);
    }
    public static void createArray()
    {
        a = new int[20];
        for (int k = 0; k < a.length; k ++)
        {
            int value = new Random().nextInt(1000);
            a[k] = value;
        }
    }
    public static void sortArray()
    {
        long startTime = System.nanoTime();
        int elements = Integer.MIN_VALUE;
        for (int k=0; k<a.length;k++)
        {
            if (a[k]>elements)
            System.out.print(a[k]+" ");
        }
        System.out.println();
        long endTime = System.nanoTime();
        System.out.println("Time Taken to Sort: " + (endTime - startTime));
    }

    public static long forTenK()
    {
        long startTime = System.nanoTime();
        int tenK = 10000;
        for (int k=1; k<=tenK;k++) {
            if (tenK% k ==1)
            System.out.println(k);
        }
        long endTime = System.nanoTime();
        long tenKDifference = endTime-startTime;
        System.out.println("Time taken for Ten Thousand Integers is "+tenKDifference);
        /*double rate = (10000.0/(double)tenKDifference)*100;
        System.out.println("Rate: "+rate+"%");*/
        return tenKDifference;
    }
    public static long forHundredK()
    {
        int b =100_000;
        long startTime = System.nanoTime();
        for (int k=1; k<=b;k++) {
           if (b%k==5)
            System.out.println(k);
        }
        long endTime = System.nanoTime();
        long hundredDifference = endTime-startTime;
        System.out.println("Time taken for Hundred Thousand Integers is "+hundredDifference);
        /*double rate = (90000.0/(double)hundredDifference)*100;
        System.out.println("Rate: "+rate+"%");*/
        return hundredDifference;
    }
    public static long forMillion()
    {
        int c= 1_000_000;
        long startTime = System.nanoTime();
        for (int k=1; k<=c;k++)
        {
            if(c%k ==17)
           System.out.println(k);
        }
        long endTime = System.nanoTime();

        long millionDifference = endTime-startTime;
        System.out.println("Time taken for Million Integers is "+millionDifference);
        /*double rate = (900000.0/(double)millionDifference)*100;
        System.out.println("Rate: "+rate+"%");*/
        return millionDifference;
    }
    public static long forTenMillion()
    {
        int tenM=10_000_000;
        long startTime = System.nanoTime();
        for (int k=1; k<=tenM;k++)
        {if (tenM%k == 19)
            System.out.println(k);
        }
        long endTime = System.nanoTime();
        long tenMDifference = endTime-startTime;
        System.out.println("Time taken for Ten Million Integers is "+tenMDifference);

        /*double rate = (9000000.0/(double) tenMDifference)*100;
        System.out.println("Rate: "+rate+"%");*/
    return tenMDifference;
    }
}
