package com.codesol.Personal;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class CheckerTest {

    @Test
    void compareTest() {

        Player player = new Player("Joseph", 89);
        Player player2 = new Player("Brian", 97);


        Assert.assertEquals(1, new Checker().compare(player, player2));
    }
    @Test
    void solutionTest()
    {
        Player[] players= new Player[5];
        players[0] = new Player("amy",100);
        players[1] = new Player("david",100);
        players[2] = new Player("aakansha",75);
        players[3] = new Player("heraldo",50);
        players[4] = new Player("aleksa",150);

        Assert.assertEquals("[aleksa 150, amy 100, david 100, aakansha 75, heraldo 50]",new Solution().solution(players));
    }
}