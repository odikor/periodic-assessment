package com.codesol.Personal;

import java.util.HashSet;
/**
 * Write a function that takes an array of numbers (integers for the tests) and a target number. It should find two different items in the
 * array that, when added together, give the target value. The indices of these items should then be returned in a tuple like so: (index1, index2).
 *
 * For the purposes of this kata, some tests may have multiple answers; any valid solutions will be accepted.
 *
 * The input will always be valid (numbers will be an array of length 2 or greater, and all of the
 * items will be numbers; target will always be the sum of two different items from that array).
 * */
public class arraySolution {

static String printPairs(int arr[], int sum)
{
    String finalIndex="";
    HashSet<Integer> s = new HashSet<>();
    for (int i = 0; i < arr.length; ++i)
    {
        int temp = sum - arr[i];
        int k;
        if (s.contains(temp)) {
            for (k=0; k<arr.length; ++k)
                if (arr[k]==temp) {
                    String indices = "(" + k + "," + i + ")"; // Create the Indices for the sum elements
                    /**
                     * @return indices
                     *
                     **/
                    if (finalIndex.length()>0)
                        finalIndex +=", ";
                    finalIndex += indices;
                }
        }
      s.add(arr[i]);
    }
    return finalIndex;
}
    public static void main(String[] args)
    {
        int numbers[] = { 1, 4, 45, 6, 10, 8, 50, 5,55,0,0,10 };
        int sum = 55;
        System.out.println(printPairs(numbers, sum));
    }
}