package com.codesol.Personal;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class SortingArraysTest {
    int[] arr = new int[]{12,45,2,89,27,4};
    int m=3;
    int n=3;
    int r=3;
    String expectedOutput ="[2, 4, 12, 27, 45, 89]";

    @Test
    void bubbleSort() {

       Assert.assertEquals(expectedOutput, Arrays.toString(new SortingArrays().bubbleSort(arr)));
    }

    @Test
    void selectionSort() {

       Assert.assertEquals(expectedOutput, Arrays.toString(new SortingArrays().selectionSort(arr)));
    }

    @Test
    void insertSort() {
        Assert.assertEquals(expectedOutput, Arrays.toString(new SortingArrays().insertSort(arr)));
    }

    @Test
    void mergeSort() {
        Assert.assertEquals(expectedOutput, Arrays.toString(new SortingArrays().mergeSort(arr)));
    }

    @Test
    void printArray() {
    }

    @Test
    void generateArray() {
    }
}