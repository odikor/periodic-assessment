package com.codesol.Personal;

import java.util.Arrays;
import java.util.Random;

public class SortingArrays {

    /**
     * @bubbleSort of an array method using a
     * @forloop
     * */
    public static int[] bubbleSort(int arr[])
    {
        for (int k=0; k<arr.length-1; k++){
            if (arr[k] > arr[k + 1]) {
                int temp = arr[k];
                arr[k] = arr[k + 1];
                arr[k + 1] = temp;
                k = -1;
            }
        }
        return arr;
    }
    /**
     * @selectionsorting of an array method
     * */
    public static int[] selectionSort(int arr[])
    {
        for (int i=0; i< arr.length; i++)
        {
            int base =i; // the base value to compare the others with
            for (int j = i+1; j< arr.length; j++)
            {
                if(arr[j]<arr[base])
                {
                    base = j;
                }
            }
            /**
             * @swapping of the arrays to sort from small to largest
             * */
            int temp = arr[base];
            arr[base]= arr[i];
            arr[i]=temp;
        }
        return arr;
    }
    /**
     * @InsertSort of an array method
     * */
    public static int[] insertSort(int arr[])
    {
        int n = arr.length;
        for (int k=1; k<n; k++)
        {
            int key = arr[k];
            int j = k - 1;

            /**  @Move_arr[0..i-1] element, that are
             * greater than key, to one position ahead
             * of their current position *
             */
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    return arr;
    }

    /**
     * @Create the Merge sort of Arrays
     * @return*/
    public static int[] mergeSort(int arr[])
    {
        int l =0,  m = 0,  r =0;
        // Find sizes of two subarrays to be merged
        int n1 = m - l + 1;
        int n2 = r - m;

        /* Create temp arrays */
        int L[] = new int[n1];
        int R[] = new int[n2];

        /*Copy data to temp arrays*/
        for (int i = 0; i < n1; ++i)
            L[i] = arr[l + i];
        for (int j = 0; j < n2; ++j)
            R[j] = arr[m + 1 + j];

        /* Merge the temp arrays */

        // Initial indexes of first and second subarrays
        int i = 0, j = 0;

        // Initial index of merged subarry array
        int k = l;
        while (i < n1 && j < n2) {
            if (L[i] <= R[j]) {
                arr[k] = L[i];
                i++;
            }
            else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        /* Copy remaining elements of L[] if any */
        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }

        /* Copy remaining elements of R[] if any */
        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }
        return arr;
    }

    // Main function that sorts arr[l..r] using
    // merge()
    void sort(int arr[], int l, int r)
    {
        if (l < r) {
            // Find the middle point
            int m =l+ (r-l)/2;

            // Sort first and second halves
            sort(arr, l, m);
            sort(arr, m + 1, r);

            // Merge the sorted halves
            mergeSort(arr);
        }
    }

    /**
     * @Print_array with space separation
     * */
   public static void printArray(int arr[])
    {
        int n = arr.length;
        for (int i = 0; i < n; ++i)
            System.out.print(arr[i] + " ");

        System.out.println();
    }
    /**
     * @Generate_random_Array to sort
     * */
    public static int[] generateArray(int arr[])
    {
        for (int k = 0; k < arr.length; k ++)
        {
            int value = new Random().nextInt(1000);
            arr[k] = value;
        }
        return arr;
    }

    public static void main(String[] args) {
        int[] arr = new int[10];
        arr = generateArray(arr);
        System.out.print("Original Array: ");
        printArray(arr);
        System.out.println();

        System.out.print("Bubble Sorting of array: ");
        printArray(bubbleSort(arr));
        System.out.print("Selection Sorting of array: ");
        printArray(selectionSort(arr));
        System.out.print("Insert Sorting of array: ");
        printArray(selectionSort(arr));
        /*System.out.print("Merge Sorting of array: ");
        printArray(mergeSort(arr));*/
    }
}
