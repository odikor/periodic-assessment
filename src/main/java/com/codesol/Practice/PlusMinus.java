package com.codesol.Practice;

public class PlusMinus {

    public static String solve(int[]a)
    {
        int positive=0, negative=0, zero=0;
        for (int k:a)
        {
            if(k==0)
            {
                zero++;
            }
            if (k<0)
            {
                negative++;
            }
             if(k>0)
            {
                positive++;
            }
        }
        double positiveRatio= (double)positive/a.length;
        double negativeRatio= (double)negative/a.length;
        double zeroRatio= (double)zero/a.length;

        return String.format("%.6f",positiveRatio)+"\n"+ String.format("%.6f",negativeRatio)+"\n"+String.format("%.6f",zeroRatio);
    }
}
