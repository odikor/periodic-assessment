package com.codesol.Practice;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlusMinusTest {

    @Test
    void solve() {
        int[] arr = new int[]{1,1,0,-1,-1};
        String expectedValue="0.400000\n0.400000\n0.200000";
        assertEquals(expectedValue, new PlusMinus().solve(arr));
    }
}